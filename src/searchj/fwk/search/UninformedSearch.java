package searchj.fwk.search;

import java.util.ArrayList;
import java.util.List;

public class UninformedSearch {
	private String _finalState;
    private String _solutionPath;
    private boolean _controlCycle;

    public UninformedSearch(boolean _controlCycle) {
        super();
        this._finalState = "";
        this._solutionPath = "";
        this._controlCycle = _controlCycle;
    }

    private boolean GetSolutionPathFromNodes(List<State> nodes){
        boolean output;
        output = false;

        for (int i = 0; i < nodes.size(); i++)
        {
            if (nodes.get(i).IsFinal()) {
                output = true;
                _solutionPath = nodes.get(i).GetPath();
                _finalState = nodes.get(i).toString();
                break;
            }
        }
        return (output);
    }

    public String GetFinalState() {
        return (_finalState);
    }
    public String GetSolutionPath() {
        return (_solutionPath);
    }

    // Breadth First Search (Algoritmo de busqueda por Amplitud)
    public boolean BFSSearch (State initialState){
        boolean output;
        List<State> nodes, visited, successors, remove;

        output = false;
        _solutionPath = "";
        _finalState = "";

        nodes = new ArrayList<State>();
        visited = new ArrayList<State>();
        successors = new ArrayList<State>();
        remove = new ArrayList<State>();

        // Agrega raíz a la lista
        nodes.add(initialState);
        State current;
        
        while ((nodes.size() > 0) && (!GetSolutionPathFromNodes (nodes))) {
            current = nodes.get(0);
            
            //Aplico control de repetidos
            if (_controlCycle) {
                visited.add(nodes.get(0)); //Agrego nodo a visitados
                nodes.remove(0); // Remueve el primer elemento
                successors.addAll(current.GetSuccessors()); //Auxiliar donde se iran removiendo sucesores ya visitados

                //Recorro lista de sucesores, y si encuentro el mismo Id en visitados, entonces debo removerlo de la lista
                for(State s1 : successors) {
                	for(State s2 : visited) {
                        if (s1.GetId().equals(s2.GetId())) {
                            System.out.println ("Ignorando nodo visitado: " + s1.GetId());
                            remove.add(s1); // Guardo los elementos a borrar luego de iterar
                        }
                    }
                }
                successors.removeAll(remove); // Elimino los elementos guardados previamente para ser borrados
                
                nodes.addAll(successors); //Agrego a la traza lista de sucesores limpia (sin repetidos)
                
            // Sin control de repetidos
            } else {
                nodes.remove(0); // Remueve el primer elemento
                nodes.addAll(current.GetSuccessors()); // Agrega sucesores al final de la lista
            }
            
            successors = new ArrayList<State>(); //Vuelvo a dejar sucesores en nulo
            remove = new ArrayList<State>();
        }

        output = GetSolutionPathFromNodes(nodes);
        nodes = null;
        return (output);
    }

    // Depth First Search (Algoritmo de busqueda por Profundidad)
    public boolean DFSSearch (State initialState){
        boolean output;
        List<State> nodes, visited, successors, remove;

        output = false;
        _solutionPath = "";
        _finalState = "";

        nodes = new ArrayList<State>();
        visited = new ArrayList<State>();
        successors = new ArrayList<State>();
        remove = new ArrayList<State>();

        // Agrega raíz a la lista
        nodes.add(initialState);
        
        State current;
        while ((nodes.size() > 0) && (!GetSolutionPathFromNodes(nodes))) {
        	current = nodes.get(0);
        	
	        // Aplico control de repetidos
	        if (_controlCycle) {
	            visited.add(nodes.get(0)); //Agrego nodo a visitados
	            nodes.remove(0); // Remueve el primer elemento
	            successors.addAll(current.GetSuccessors()); //Auxiliar donde se iran removiendo sucesores ya visitados
	
	            //Recorro lista de sucesores, y si encuentro el mismo Id en visitados, entonces debo removerlo de la lista
	            for(State s1 : successors) {
	            	for(State s2 : visited) {
	                    if (s1.GetId().equals(s2.GetId())) {
	                        System.out.println ("Ignorando nodo visitado: " + s1.GetId());
	                        remove.add(s1); // Guardo los elementos a borrar luego de iterar
	                    }
	                }
	            }
	            successors.removeAll(remove); // Elimino los elementos guardados previamente para ser borrados
	            
	            nodes.addAll(0, successors); //Agrego a la traza lista de sucesores limpia (sin repetidos)
	            
	        // Sin control de repetidos
	        } else {
	            nodes.remove(0); // Remueve el primer elemento
	            nodes.addAll(0, current.GetSuccessors()); // Agrega sucesores al final de la lista
	        }
        }
	        
        output = GetSolutionPathFromNodes(nodes);
        nodes = null;
        return (output);
    }

    private boolean DLSSearch(State initialState, int maxDepth){
        boolean output;
        List<State> nodes;

        output = false;
        _solutionPath = "";
        _finalState = "";

        nodes = new ArrayList<State>();

        // Agrega raíz a la lista
        nodes.add(initialState);

        while ((nodes.size() > 0) && (!GetSolutionPathFromNodes(nodes)))
        {
            State current;

            current = nodes.get(0);

            // Remueve el primer elemento
            nodes.remove(0);

            if (current.GetDepth () < maxDepth)
                // Agrega sucesores al inicio de la lista
                nodes.addAll(0, current.GetSuccessors());

            current = null;
        }

        output = GetSolutionPathFromNodes(nodes);

        nodes = null;

        return (output);
    }

    // Iterative Deeping Search (Algoritmo de busqueda por Profundidad Iterativa)
    public boolean IDSSearch (State initialState){
        boolean output;
        int maxDepth;

        output = false;
        maxDepth = 0;

        while (!output) {
            output = DLSSearch(initialState, maxDepth);
            maxDepth++;
        }

        return (output);
    }
}
