import searchj.fwk.problemstate.WaterState;
import searchj.fwk.search.UninformedSearch;

public class Main {

	public static void main(String[] args) {
        boolean hasSolution;
        UninformedSearch search;
        WaterState initialWaterState;

        search = new UninformedSearch(true);
        initialWaterState = new WaterState(0, 0);

        hasSolution = search.BFSSearch(initialWaterState);
//        hasSolution = search.DFSSearch(initialWaterState);

        if (hasSolution) {
            System.out.printf(String.format("La solución al problema encontrada por BFS es:\n%s", search.GetSolutionPath()));
        } else {
            System.out.printf("BFS no encontró solución al problema.");
        }

        initialWaterState = null;
        search = null;
    }
}
