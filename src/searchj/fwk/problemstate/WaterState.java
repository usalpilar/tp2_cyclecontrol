package searchj.fwk.problemstate;

import searchj.fwk.search.State;

import java.util.ArrayList;
import java.util.List;

public class WaterState extends State {
	private int _jar4;
    private int _jar3;
    private String _path;
    private String _id; //Id donde se concatenará estado de la jarra de 4 litros | estado de la jarra de 3 litros

    public WaterState(int jar4, int jar3) {
        super();
        _jar4 = jar4;
        _jar3 = jar3;
        _path = "";
        _id = ConcatenateStates(); //Agrego a constructor asignación del Id al momento de ser instanciada
    }

    public WaterState(int jar4, int jar3, String path) {
        super();
        _jar4 = jar4;
        _jar3 = jar3;
        _path = path;
        _id = ConcatenateStates(); //Agrego a constructor asignación del Id al momento de ser instanciada
    }

    //Método generador de Id
    private String ConcatenateStates () {
        return (String.valueOf(_jar4)+"|"+String.valueOf(_jar3));
    }
    
    // Método para obtener la profundidad de un nodo
    public int GetDepth() {
        if (_path == "")
            return (0);

        return (_path.split("|").length);
    }

    //Método para obtener el camino hacia un nodo
    public String GetPath() {
        return (_path);
    }
    
    //Método para obtener el id de cada nodo dependiendo de su estado
    public String GetId() {
        return (_id);
    }

    public List<State> GetSuccessors() {
        ArrayList<State> output;

        output = new ArrayList<State>();

        // 1) Llena la jarra de 4
        if (_jar4 < 4)
            output.add(new WaterState(4, _jar3, _path + "|" + "LLenar la jarra de 4."));
        // 2) Llena la jarra de 3
        if (_jar3 < 3)
            output.add(new WaterState(_jar4, 3, _path + "|" + "Llenar la jarra de 3."));
        // 3) Vacía la jarra de 4
        if (_jar4 > 0)
            output.add(new WaterState(0, _jar3, _path + "|" + "Vaciar la jarra de 4."));
        // 4) Vacía la jarra de 3
        if (_jar3 > 0)
            output.add(new WaterState(_jar4, 0, _path + "|" + "Vaciar la jarra de 3."));
        // 5) Vacía la jarra de 4 en la de 3
        if ((_jar4 > 0) && ((_jar4 + _jar3) <= 3))
            output.add(new WaterState(0, _jar4 + _jar3, _path + "|" + "Vaciar la jarra de 4 en la de 3."));
        // 6) Vacía la jarra de 3 en la de 4
        if ((_jar3 > 0) && ((_jar4 + _jar3) <=4))
            output.add(new WaterState(_jar4 + _jar3, 0, _path + "|" + "Vaciar la jarra de 3 en la de 4."));
        // 7) Traspasa la jarra de 4 a la de 3
        if ((_jar4 > 0) && ((_jar4 + _jar3) > 3))
            output.add(new WaterState(_jar4 - (3 - _jar3), 3, _path + "|" + "Traspasar la jarra de 4 a la de 3."));
        // 8) Traspasa la jarra de 3 a la de 4
        if ((_jar3 > 0) && ((_jar4 + _jar3) > 4))
            output.add(new WaterState(4, _jar3 - (4 - _jar4), _path + "|" + "Traspasar la jarra de 3 a la de 4."));

        return (output);
    }

    public boolean IsFinal() {
        return (_jar4 == 2);
    }
}