package searchj.fwk.search;

import java.util.List;

public abstract class State {
    public abstract int GetDepth();
    public abstract String GetPath();
    public abstract String GetId();
    public abstract List<State> GetSuccessors();
    public abstract boolean IsFinal();
}
